FROM node:4
MAINTAINER Pierre-Jean Vardanega <pierrejean.vardanega@gmail.com>

RUN npm install babel webpack webpack-dev-server -g
RUN npm install react react-dom --save
RUN npm install babel-loader babel-core babel-preset-es2015 babel-preset-react

RUN apt-get update && apt-get -y install apt-transport-https

RUN wget -q -O - https://packages.cloudfoundry.org/debian/cli.cloudfoundry.org.key | apt-key add -
RUN echo "deb http://packages.cloudfoundry.org/debian stable main" | tee /etc/apt/sources.list.d/cloudfoundry-cli.list

RUN apt-get update && apt-get -y install cf-cli
